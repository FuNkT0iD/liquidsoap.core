#!/bin/bash

nohup /home/liquidice/.opam/system/bin/liquidsoap /home/liquidice/conf/radio.liq &
nohup /home/liquidice/icecast-2.4.4/src/icecast -c /home/liquidice/conf/icecast.xml &
tail -f /dev/null