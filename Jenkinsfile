pipeline { 
    agent any 
    environment {
        NOW = new Date().format("yyyyMMddHHmm", TimeZone.getTimeZone('UTC'))
        PRIMARYBRANCH = "trunk"
        APPNAME = "LiquidSoap.Core"
        QUBE = "http://data.rebiszindustries.com:9000/dashboard?id=liquidsoapcore"
    }
    options {
        skipStagesAfterUnstable()
    }
    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }
    stages {
        stage ('Clean workspace') 
        {
            steps {
                cleanWs()
            }
        }
        stage ('Checkout') 
        {
            steps {
                checkout(
                    [
                        $class: 'GitSCM', 
                        branches: [[name: '**']], 
                        //branch: env.BRANCH_NAME,
                        browser: [$class: 'GitLabBrowser'], 
                        extensions: [], 
                        userRemoteConfigs: [
                            [
                                credentialsId: 'GitLab_grebisz_UserPass', 
                                url: 'https://gitlab.com/FuNkT0iD/liquidsoap.core.git'
                            ]
                        ]]
                )
                
           }
        }
        stage ('Test')
        {
            steps {
                withSonarQubeEnv("RebiszIndustries_data") {
                    script{
                        try{
                            sh "dotnet tool install --global dotnet-sonarscanner --version 5.2.2"
                        }
                        catch(Exception ex){

                        }    
                    }
                    sh "/root/.dotnet/tools/dotnet-sonarscanner begin /k:'liquidice' /d:sonar.login='42a789bed3a0c75af80e54f9f94f1b1bbd2d80b6' -d:sonar.host.url='http://data.rebiszindustries.com:9000/' -d:sonar.cs.opencover.reportsPaths='./LiquidSoap.Core.Tests/coverage.opencover.xml'"
                    sh "dotnet clean && dotnet build && dotnet test ./LiquidSoap.Core.Tests/LiquidSoap.Core.Tests.csproj --logger:trx /p:CollectCoverage=true /p:CoverletOutputFormat=opencover"
                    sh "/root/.dotnet/tools/dotnet-sonarscanner end /d:sonar.login='42a789bed3a0c75af80e54f9f94f1b1bbd2d80b6'"
                }
                mstest testResultsFile:"**/**/*.trx", keepLongStdio: true
            }
        }
        stage("Quality gate") {
            steps {
                waitForQualityGate abortPipeline: true
            }
        }
        stage('publish')
        {
            steps{
                script {
                    if (env.BRANCH_NAME == env.PRIMARYBRANCH) {
                        sh "dotnet pack ./LiquidSoap.Core/LiquidSoap.Core.csproj --version-suffix ${env.BRANCH_NAME}${env.NOW}"
                        sh "dotnet nuget push ./LiquidSoap.Core/bin/Debug/*.nupkg -k ${env.LiquidSoapCore_Nuget} -s https://api.nuget.org/v3/index.json"
                    }
                }
            }
        }
    }
    post {
        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }
    }
}