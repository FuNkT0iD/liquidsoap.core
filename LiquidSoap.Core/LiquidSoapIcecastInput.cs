namespace LiquidSoap.Core
{
    public class LiquidSoapIcecastInput : ILiquidSoapObject{
        public string name {get; set;}
        public bool isConnected {get;set;}
        public LiquidSoapIcecastInput(string name)
        {
            this.name = name;
        }

        public string Status(){
            return name + ".status"; 
        }
    }
}
