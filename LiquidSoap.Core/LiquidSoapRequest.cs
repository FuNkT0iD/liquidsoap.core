namespace LiquidSoap.Core
{
    public class LiquidSoapRequest : ILiquidSoapObject{
        public string name {get; set;}

        public LiquidSoapRequest(string name)
        {
            this.name = name;
        }
        public string Push(string filepath){
            return name + ".push " + filepath;
        }
        public string PendingLength(){
            return name + ".pending_length"; 
        }
        public string Queue(){
            return name + ".queue";
        }
    }
}
