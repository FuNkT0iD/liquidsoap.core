namespace LiquidSoap.Core
{
    public class LiquidSoapConfig
    {
        public int Port {get;set;}
        public string LiquidSoapHost {get;set;}
        public string HivePath {get;set;}
        public string HiveMappedPath {get;set;}
        public string RecPath {get;set;}
        public string LiveRecPath {get;set;}
        public bool AutomaticRecording {get;set;} = true;
        public bool EnableAutoDj {get;set;} = true;
    }
}