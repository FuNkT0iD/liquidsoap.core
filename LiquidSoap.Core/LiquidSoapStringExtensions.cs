namespace LiquidSoap.Core
{
    public static class LiquidSoapStringExtensions
    {
        public static string Sanitize(this string input)
        {
            return input.Replace("\r", "").Replace("\n", "").Replace("END", "");
        }
    }
    
}