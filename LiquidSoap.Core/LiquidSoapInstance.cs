using System;
using System.Linq;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LiquidSoap.Core
{
    public class LiquidSoapInstance
    {
        public int port {get; private set;}
        public string host {get; private set;}
        public string recpath {get; private set;}
        public string liverecpath {get; private set;}
        public List<LiquidSoapRequest> requestq {get; private set;}
        public List<LiquidSoapIcecastOutput> _outputs {get; private set;}
        public List<LiquidSoapIcecastInput> _inputs {get; private set;}
        public List<LiquidSoapFile> _recordingfiles {get; private set;}

        public event EventHandler<LiquidSoapFile> RecordingStarted;
        public event EventHandler<string> RecordingStopped;

        public LiquidSoapInstance(int prt, string hst, string rcpath, string liverec){
            port = prt;   
            host = hst;
            recpath = rcpath;
            liverecpath = liverec;
            RefreshCommands().GetAwaiter().GetResult();
        }
        // Request Queue Management
        public async Task QueuePush(string filepath, string queueName = ""){
            LiquidSoapRequest requestQueue = GetRequestByName(queueName);
            if(requestQueue != null) await Flush(requestQueue.Push(filepath) + "\n");
        }
        public async Task<int> QueuePendingLength(string queueName = ""){
            LiquidSoapRequest requestQueue = GetRequestByName(queueName);
            return Int32.Parse((await Flush(requestQueue.PendingLength() + "\n")));
        }
        public async Task<List<int>> Queue(string queueName = ""){
            LiquidSoapRequest requestQueue = GetRequestByName(queueName);
            List<int> intList = new List<int>();
            string[] str = (await Flush(requestQueue.Queue() + "\n")).Split(' ');
            for(var i=0;i<str.Length;i++)
            {
                if(!String.IsNullOrWhiteSpace(str[i]))
                {
                    intList.Add(Int32.Parse(str[i]));
                }
            }
            return intList;
        }
        // Icecast Output Management
        public async Task<double> OutputTimeRemaining(string outputName = ""){
            LiquidSoapIcecastOutput ice = GetOutputByName(outputName);
            var remaining = (await Flush(ice.GetRemaining() + "\n"));
            double converted = 0;
            Double.TryParse(remaining, out converted);
            return converted;
        }
        public async Task<string> OutputSkip(string outputName = ""){
            
            LiquidSoapIcecastOutput ice = GetOutputByName(outputName);
            return await Flush(ice.Skip() + "\n");
        }
        public Task<string> OutputStart(string outputName = ""){
            LiquidSoapIcecastOutput ice = GetOutputByName(outputName);
            return Flush(ice.Start() + "\n");
        }
        public Task<string> OutputStop(string outputName = ""){
            LiquidSoapIcecastOutput ice = GetOutputByName(outputName);
            return Flush(ice.Stop() + "\n");
        }
        public Task<string> OutputStatus(string outputName = ""){
            LiquidSoapIcecastOutput ice = GetOutputByName(outputName);
            return Flush(ice.Status() + "\n");
        }
        // Icecast Input Management
        public Task<string> InputStatus(string inputName = "")
        {
            var input = GetInputByName(inputName);    
            return Flush(input.Status() + "\n");
        }
        // File Management
        public Task<string> RecordingStatus(string recordingName = ""){
            var recording = GetRecordingByName(recordingName);
            return Flush(recording.Status() + "\n");
        }
        public Task<string> RecordingStart(string recordingName = ""){
            var recording = GetRecordingByName(recordingName);
            RecordingStarted?.Invoke(this, recording);
            return Flush(recording.Start() + "\n");
        }
        public async Task<string> RecordingStop(string recordingName = "", string artist="unkown", string show="unknown"){
            var recording = GetRecordingByName(recordingName);
            await Flush(recording.Stop() + "\n");
            string oldpath = recording.FilePath().Replace("/home/liquidice/recordings/", recpath);
            RecordingStopped?.Invoke(this, oldpath);
            return oldpath;
        }
        
        #region Helper Methods
        private LiquidSoapIcecastInput GetInputByName(string inputname)
            => GetBestMatching(inputname, _inputs);

        private LiquidSoapIcecastOutput GetOutputByName(string outputname)
            => GetBestMatching(outputname, _outputs);

        private LiquidSoapRequest GetRequestByName(string queueName)
            => GetBestMatching(queueName, requestq);

        private LiquidSoapFile GetRecordingByName(string recordingName)
            => GetBestMatching(recordingName, _recordingfiles);

        private T GetBestMatching<T>(string search, List<T> objects) where T : ILiquidSoapObject
        {
            T item = default(T);
            if(search.Replace(" ", "") != "")
            {
                item = objects.FirstOrDefault(x => x.name == search);
            }
            return item == null ? objects.FirstOrDefault() : item;
        }
        #endregion
        
        #region Flushing

        private async Task RefreshCommands(){
            requestq = new List<LiquidSoapRequest>();
            _outputs = new List<LiquidSoapIcecastOutput>();
            _inputs = new List<LiquidSoapIcecastInput>();
            _recordingfiles = new List<LiquidSoapFile>();
            string result = await Flush("help\n", false);
            foreach(string s in result.Split('\n'))
            {
                ParseLiquidSoapLine(s);
            }
        }

        private void ParseLiquidSoapLine(string s){
            var extracted = ExtractItem(s.Replace("\r", ""));
            if(s.StartsWith("| requests") && !requestq.Any(x => x.name == extracted))
            {
                requestq.Add(new LiquidSoapRequest(extracted));                 
            }
            if(s.StartsWith("| output") && !_outputs.Any(x => x.name == extracted)){
                _outputs.Add(new LiquidSoapIcecastOutput(extracted));                    
            }
            if(s.StartsWith("| /") && !_recordingfiles.Any(x => x.name == extracted)){
                _recordingfiles.Add(new LiquidSoapFile(extracted));
            }
            if(s.StartsWith("| input") && !_inputs.Any(x => x.name == extracted)){
                LiquidSoapIcecastInput icecastInput = new LiquidSoapIcecastInput(extracted);
                string inputstatus = Flush(icecastInput.Status() + "\n").GetAwaiter().GetResult();
                if(inputstatus.ToLower().StartsWith("client connected "))
                {
                    icecastInput.isConnected = true;
                }
                else
                {
                    icecastInput.isConnected = false;
                }
                _inputs.Add(icecastInput);
            }
        }

        private string ExtractItem(string s)
        {
            if(s.Any(x => x == '.'))
            {
                var split = s.Substring(2).Split('.');
                return split[0];
            }
            return s;
        }
        private async Task<string> Flush(string message, bool sanitize = true){
            var response = "";
            using(TcpClient tcp = new TcpClient(host, port))
            {
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
                using(NetworkStream stream = tcp.GetStream())
                {
                    await stream.WriteAsync(data,0,data.Length);
                    data = new Byte[1000000];
                    Int32 bytes = await stream.ReadAsync(data, 0, data.Length);
                    response = System.Text.Encoding.ASCII.GetString(data, 0, bytes); 
                    return sanitize ? response.Sanitize() : response;
                }
            }
            
        }

        #endregion
    }
}