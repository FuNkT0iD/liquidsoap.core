namespace LiquidSoap.Core{
    public class LiquidSoapCore{
        public LiquidSoapInstance ls {get;set;}
        
        private bool autoRecord {get;set;}
        private bool isRecording {get;set;}
        public LiquidSoapCore(LiquidSoapConfig config){
            ls = new LiquidSoapInstance(config.Port, config.LiquidSoapHost, config.RecPath, config.LiveRecPath);
            autoRecord = config.AutomaticRecording;
            isRecording = false;
        }
    }
}