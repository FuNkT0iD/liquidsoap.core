namespace LiquidSoap.Core{
    /// <summary>
    /// The definition of a file used for outputting recorded audio
    /// within a stream
    /// </summary>
    public class LiquidSoapFile : ILiquidSoapObject{
        public string name {get; set;}

        public LiquidSoapFile(string f)
        {
            name = f;
        }

        public string FilePath()
        {
            return name.Replace("(dot)", ".");
        }

        public string Start()
        {
            return name + ".start";
        }
        public string Stop(){
            return name + ".stop";
        }
        public string Status(){
            return name + ".status";
        }
    }
}