namespace LiquidSoap.Core
{
    public class LiquidSoapIcecastOutput : ILiquidSoapObject{
        public string name {get; set;}

        public LiquidSoapIcecastOutput(string name)
        {
            this.name = name;
        }

        public string GetRemaining(){
            return name + ".remaining";
        }

        public string Skip(){
            return name + ".skip";
        }

        public string Start(){
            return name + ".start";
        }

        public string Stop(){
            return name + ".stop";
        }

        public string Status(){
            return name + ".status";
        }
    }
}
