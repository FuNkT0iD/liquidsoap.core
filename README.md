# LiquidSoap.Core

This is a wrapper library for easy communication with LiquidSoap (by Savonet) via it's daemonized port.

For more information see:

[LiquidSoap - Github](https://github.com/savonet/liquidsoap)

[LiquidSoap - Project](https://www.liquidsoap.info/)

[LiquidSoap - Telnet via .liq file](https://www.liquidsoap.info/doc-dev/server.html)

## Docker.LiquidIce - Optional for testing

This contains a docker image that bundles IceCast and LiquidSoap.

This uses an ubuntu 18.04 image that downloads and compiles icecast and liquidsoap (with SSL support).

### Docker Registery

The published version of this container image can be found here:
[LiquidIce - Docker Hub](https://hub.docker.com/repository/docker/funkt0id/liquidice)

It is recommended to use a docker-compose file due to the number of arguements used for configuration

You will need:
1. An Icecast configuration file - an example of which can be found in Docker.LiquidIce.conf/icecast.xml

2. A LiquidSoap liq file - an example of which can be found in Docker.LiquidIce.conf/radio.liq

3. The docker compose file
```
version: "3"
services:
    liquidice:
        container_name: liquidice
        image: funkt0id/liquidice:latest
        user: "liquidice"
        working_dir: /home/liquidice/icecast-2.4.4/src/
        entrypoint: "/home/liquidice/entrypoint.sh"
        ports:
            - "8000:8000"
            - "1234:1234"
        volumes:
            - "./conf:/home/liquidice/conf/"
            - "./logs:/var/log/icecast2/"
            - "./ssl:/home/liquidsoap/ssl/"
```

This will pull the image from the docker hub.

Ports are:

    1. 8000 - For icecast output - changing the corresponding port in icecast.xml will affect this.

    2. 1234 - For liquidsoap interaction - changing the port number in the radio.liq file will affect this.

    3. Volumes

    3.1 /home/liquidice/conf/

        This is a folder containing configuration for liquidice - both the icecast.xml as well as radio.liq reside here

    3.2 /var/log/icecast2/

        This is the directory for icecast2 logs

    3.3 /home/liquidsoap/ssl/

        This folder is used to hold certificates to allow for SSL connections.

        Please note, for this to work:

        3.3.1 You need to modify the icecast.xml with:

                ```<ssl-certificate>/home/liquidice/ssl/{YOUR CERT NAME HERE}</ssl-certificate>```

        3.3.2 For letsencrypt users:

                ```cat /etc/letsencrypt/live/YOUR DOMAIN/fullchain.pem /etc/letsencrypt/live/YOUR DOMAIN/privkey.pem > ssl/bundle.pem```
                
        3.3.3 PLEASE NOTE: /home/liquidice/ssl/ and "ssl/" in 3.3.2 are the same mapped directory from your docker-compose file


        3.4 /home/liquidsoap/hive/

       This is a folder which should contain audio files that can be used to queue to LiquidSoap / IceCast

## LiquidSoap.Core.Tests

To run unit tests - aside from having a working .NET development environment. You will need to change the configuraiton at the top of the unit test file "LiquidSoapInstanceTests.cs"