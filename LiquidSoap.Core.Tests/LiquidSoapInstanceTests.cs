using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LiquidSoap.Core.Tests
{
    [TestClass]
    public class LiquidSoapInstanceTests
    {
        private string recordingsFolder = "/home/liquidice/recordings/";
        private string recordingOutputFile = "/home/liquidice/recordings/OutputSomething.mp3";
        private string host = "liquidice";
        private int port = 1234;

        private LiquidSoapInstance ls = null;

        [TestInitialize]
        public void Init()
        {
            ls = new LiquidSoapInstance(port, host, recordingsFolder, recordingOutputFile);
        }

        [TestMethod]
        public async Task QueuePendingLength()
        {
            var qName = "requests";
            var PendingLength = await ls.QueuePendingLength(qName);
            Assert.AreEqual(0, PendingLength);
        }

        [TestMethod]
        public async Task QueuePendingLength_WrongName()
        {
            var qName = "requestarrrg";
            var PendingLength = await ls.QueuePendingLength(qName);
            Assert.AreEqual(0, PendingLength);
        }

        [TestMethod]
        public async Task QueuePendingLength_NoName()
        {
            var qName = "";
            var PendingLength = await ls.QueuePendingLength(qName);
            Assert.AreEqual(0, PendingLength);
        }

        [TestMethod]
        public async Task InputStatus()
        {
            var inputname = "input";
            var s = await ls.InputStatus(inputname);
            Assert.AreEqual("no source client connected", s);
        }

        [TestMethod]
        public async Task OutputTimeRemaining()
        {
            Assert.IsTrue(await ls.OutputTimeRemaining() > 0);
        }

        [TestMethod]
        public async Task OutputStatusTests()
        {
            var outputname = "output(dot)ogg";
            var stat = await ls.OutputStatus(outputname);
            Assert.IsTrue(stat == "on");
            await ls.OutputStop(outputname);
            System.Threading.Thread.Sleep(500);
            Assert.AreEqual("off", await ls.OutputStatus(outputname));
            await ls.OutputStart(outputname);
            System.Threading.Thread.Sleep(500);
            Assert.AreEqual("on", await ls.OutputStatus(outputname));
        }

        [TestMethod]
        public async Task RecordingTests()
        {
            Assert.AreEqual("off", await ls.RecordingStatus());
            Assert.AreEqual("OK", await ls.RecordingStart());
            System.Threading.Thread.Sleep(500);
            Assert.AreEqual("on", await ls.RecordingStatus());           
            Assert.AreEqual(recordingOutputFile, await ls.RecordingStop());
            System.Threading.Thread.Sleep(500);
            Assert.AreEqual("off", await ls.RecordingStatus());
        }

        [TestMethod]
        public async Task QueueTests()
        {
            Assert.AreEqual(0, (await ls.Queue()).Count);
            string filename = "/home/liquidice/hive/track1.mp3";
            await ls.QueuePush(filename);
            System.Threading.Thread.Sleep(500);
            Assert.AreNotEqual(0, (await ls.Queue()).Count);
        }
    }
}